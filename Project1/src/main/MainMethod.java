package main;

public class MainMethod {
	 public static void main() {  // first main method 
		 System.out.println(" Thi is  regular method .... output ..");
	 }
	 public static void main(int a) {
		 System.out.println( " This is another main method ..... ");
	 }

	public static void main(String[] martinlutherking) { // execution starts from here
		// public -- access modifier---- access to all
		// static  -- call without any object 
		// void -- return type   --- it doesnt return any value 
		// main  -- name of method 
		// String[] -- array of String type arguments 
		// args  -- just a parameter name ..
		System.out.println(" This is main method..");
		main();
		main(20);

		
		

	}

}
