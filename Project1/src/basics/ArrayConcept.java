package basics;

public class ArrayConcept {

	public static void main(String[] args) {
		//array can store multiple values
		
         int [] age = {2205,19,12,45,90,78,5,23,25};
         System.out.println(age[1]);
         System.out.println(age.length);
         
          
         String [] cities = {"london","sydney","newyork","copenhagen"};
     
         System.out.println(cities[2]);
         
   
	}
}

