package basics;

public class First {   // class created 
	
	  //method creation
	public void mymethod() {
		   // block of code or method body
		System.out.println(" My first method logic ... ");
	}
	
	public void method2() {
		System.out.println(" My second method logic .............................");
	}
	public static void main(String[] args) {
		System.out.println(" This is my first JAVA program ");
		
		// object creation 
		First abc = new First(); 
		
		abc.method2();
		abc.mymethod();

	}

}


